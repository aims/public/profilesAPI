﻿using AIMSapi;

using Elasticsearch.Net;
using GitLabApiClient;
using GitLabApiClient.Models.Branches.Requests;
using GitLabApiClient.Models.Commits.Requests.CreateCommitRequest;
using GitLabApiClient.Models.MergeRequests.Requests;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Nest;
using System;

using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

using System.Threading.Tasks;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using VDS.RDF.Query.Datasets;
using VDS.RDF.Shacl;


namespace AIMS.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AIMS : ControllerBase
    {
        private readonly VirtuosoConnector c;
        AIMSapi.MimeType MT = new();
        ElasticSearch ESClass;

        //triplestore of the vocabulary
        IGraph vocabulary = new Graph();

        //triplestore of APs
        //IGraph APs = new Graph();

        //triplestore of inheritance relations
        IGraph Inheritance = new Graph();

        //triplestore of number of children
        IGraph children = new Graph();

        //triplestore of passwords
        IGraph uuid = new Graph();

        //triplestore of APs
        IGraph states = new Graph();

        IGraph MergRequestID = new Graph();

        IGraph underReview = new Graph();

        IGraph Approvals = new Graph();

        //    IGraph versions = new Graph();

        IGraph version = new Graph();

        IGraph Community = new Graph();

        ShapesGraph APShape;


        ElasticClient ESclient;
        ConnectionSettings settings;

        Inheritance InheritC;
        /*
        InMemoryDataset dsap;
        ISparqlQueryProcessor processorap;
        SparqlQueryParser sparqlparser = new SparqlQueryParser();
        */


        private readonly ILogger<AIMS> _logger;
        private readonly IConfiguration config;

        public AIMS(ILogger<AIMS> logger, IConfiguration configuration)
        {
            _logger = logger;
            config = configuration;
            c = new(config.GetValue("AIMS_SPARQL_ENDPOINT", "http://localhost:8890/sparql"));
            c.ReadWriteSparqlConnector.LoadGraph(vocabulary, new Uri("http://www.example.org/AIMS/Vocabulary/"));
            c.ReadWriteSparqlConnector.LoadGraph(Inheritance, new Uri("http://www.example.org/AIMS/AP/Inheritance/"));
            c.ReadWriteSparqlConnector.LoadGraph(children, new Uri("http://www.example.org/AIMS/AP/children/"));
            c.ReadWriteSparqlConnector.LoadGraph(uuid, new Uri("http://www.example.org/AIMS/AP/uuid/"));
            c.ReadWriteSparqlConnector.LoadGraph(states, new Uri("http://www.example.org/AIMS/AP/states/"));
            c.ReadWriteSparqlConnector.LoadGraph(version, new Uri("http://www.example.org/AIMS/AP/version/"));
            c.ReadWriteSparqlConnector.LoadGraph(Community, new Uri("http://www.example.org/AIMS/AP/Community/"));
            c.ReadWriteSparqlConnector.LoadGraph(MergRequestID, new Uri("http://www.example.org/AIMS/AP/MergRequestID/"));
            c.ReadWriteSparqlConnector.LoadGraph(underReview, new Uri("http://www.example.org/AIMS/AP/underReview/"));
            c.ReadWriteSparqlConnector.LoadGraph(Approvals, new Uri("http://www.example.org/AIMS/AP/Approvals/"));

            IGraph gr = new Graph();
            c.ReadWriteSparqlConnector.LoadGraph(gr, new Uri("http://www.example.org/AIMS/shape/"));
            APShape = new ShapesGraph(gr);

            /* dsap = new InMemoryDataset(Inheritance);
             processorap = new LeviathanQueryProcessor(dsap);
            */

            settings = new ConnectionSettings(new Uri(config.GetValue("AIMS_ELASTIC_ENDPOINT", "http://localhost:9200"))).DefaultIndex("aims");
            ESclient = new ElasticClient(settings);
            ESClass = new ElasticSearch(ESclient);
            
            ESClass.config();
            ESclient.Indices.Refresh();
            
            InheritC = new Inheritance(Inheritance, children, c);
        }

        [HttpPut("application-profiles")]
        [Consumes("application/json")]

        public APModel AddAP([FromBody] APModel ap)
        {
            APModel res = new();

            //submit button in demo
            if (ap.state == 0)
            {
                res.accessToken = save(ap);
            }
            /*    else if (ap.state == 1)
                {
                    close(ap.base_url , ap.accessToken);
                }*/

            //publish button
            else if (ap.state == 1)
            {
                publish(ap.base_url, ap.accessToken);
            }

            IGraph h = new Graph();

            c.ReadWriteSparqlConnector.LoadGraph(h, ap.base_url);

            res.description = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")).First().Object.ToString();

            res.creator = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();

            res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

            try
            {
                res.subject = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();
            }
            catch
            {
                res.subject = "NoSubject";
            }
            res.base_url = h.BaseUri.ToString();

            res.mimeType = "text/turtle";
            //en or de
            res.name = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")).First().Object.ToString().Replace("@en", "");
            // several community problem
            res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());

            var projTriples = Approvals.GetTriplesWithSubject(h.BaseUri);
            var projTriples2 = underReview.GetTriplesWithSubject(h.BaseUri);

            List<string> projects = new List<string>();
            List<string> projects2 = new List<string>();


            foreach (Triple triple in projTriples)
            {

                projects.Add(triple.Object.ToString());
            }
            foreach (Triple triple in projTriples2)
            {

                projects2.Add(triple.Object.ToString());
            }
            res.approvedBy = projects;
            res.underReviewBy = projects2;

            List<string> child = new List<string>();

            foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
            { child.Add(triple.Subject.ToString()); }

            res.children = child;

            return res;
        }


        [HttpPut("application-profiles/Drafts/Save")]
        [Consumes("application/json")]

        public String save([FromBody] APModel ap)
        {

            if (states.GetTriplesWithSubject(UriFactory.Create(ap.base_url)).IsNullOrEmpty())
            {
                String URI = ap.base_url;

                IGraph g = new Graph();
                IRdfReader reader = MimeTypesHelper.GetParser(ap.mimeType);

                StringParser.Parse(g, ap.definition.ToString(), reader);

                g.BaseUri = new Uri(URI);

                c.ReadWriteSparqlConnector.SaveGraph(g);


                String uid = Guid.NewGuid().ToString();
                Graph ug = new Graph();
                IUriNode su = ug.CreateUriNode(g.BaseUri);
                IUriNode pre = ug.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/identifier"));
                ILiteralNode ob = ug.CreateLiteralNode(uid);
                ug.Assert(new Triple(su, pre, ob));
                uuid.Merge(ug);
                c.ReadWriteSparqlConnector.SaveGraph(uuid);

                Graph st = new Graph();
                su = st.CreateUriNode(g.BaseUri);
                pre = st.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/type"));
                ob = st.CreateLiteralNode("0");
                st.Assert(new Triple(su, pre, ob));
                states.Merge(st);
                c.ReadWriteSparqlConnector.SaveGraph(states);

                // creating a new ap successfully
                return uid;
            }
            else if (states.GetTriplesWithSubject(UriFactory.Create(ap.base_url)).First().Object.ToString() == "0")
            {

                if (uuid.GetTriplesWithSubject(new Uri(ap.base_url)).First().Object.ToString() == ap.accessToken)
                {
                    String URI = ap.base_url;

                    IGraph g = new Graph();
                    IRdfReader reader = MimeTypesHelper.GetParser(ap.mimeType);

                    StringParser.Parse(g, ap.definition.ToString(), reader);

                    g.BaseUri = new Uri(URI);

                    c.ReadWriteSparqlConnector.SaveGraph(g);
                    return ap.accessToken;
                }
                else
                    //ID is wrong!
                    return "-1";

            }
            else
            {
                //update is not possible!
                return "-1";
            }
        }

        [HttpGet("application-profiles/state")]

        public List<APModel> getall(string state)

        {
            List<APModel> listap = new List<APModel>();
            var profiles = states.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/type"));

            foreach (Triple i in profiles)
            {
                if (i.Object.ToString() == state)
                {
                    APModel res = new();

                    IGraph h = new Graph();
                    c.ReadWriteSparqlConnector.LoadGraph(h, i.Subject.ToString());

                    res.definition = MT.TypeConvertor("Turtle", h);

                    listap.Add(res);
                }
            }
            //List<APModel> objListOrder =
            //listap.OrderBy(order => order.numInherit).ToList();

            return listap;

        }

        [HttpGet("application-profiles/Drafts/Publish")]

        public void publish(String URI, string id)

        {
            if (states.GetTriplesWithSubject(UriFactory.Create(URI)).First().Object.ToString() == "0")
            {
                if (uuid.GetTriplesWithSubject(new Uri(URI)).First().Object.ToString() == id)
                {

                    states.Retract(states.GetTriplesWithSubject(UriFactory.Create(URI)).First());
                    c.ReadWriteSparqlConnector.SaveGraph(states);
                    Graph st = new Graph();
                    IUriNode su = st.CreateUriNode(UriFactory.Create(URI));
                    IUriNode pre = st.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/type"));
                    ILiteralNode ob = st.CreateLiteralNode("1");
                    st.Assert(new Triple(su, pre, ob));
                    states.Merge(st);
                    c.ReadWriteSparqlConnector.SaveGraph(states);

                    IGraph g = new Graph();
                    c.ReadWriteSparqlConnector.LoadGraph(g, new Uri(URI));

                    //        APs.Merge(g);
                    //        c.ReadWriteSparqlConnector.SaveGraph(APs);

                    string s = "";
                    string t = "";
                    string sd = "";
                    string td = "";

                    foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))
                    {
                        string s1 = item.Object.ToString();
                        if (s1.Contains("@en"))
                        {
                            t = s1.Replace("@en", "");

                            s = s + t + " ";
                        }
                        else if (s1.Contains("@de"))
                        {
                            td = s1.Replace("@de", "");

                            sd = sd + td + " ";
                        }
                    }


                    string d = "";
                    string dd = "";
                    foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description"))
)
                    {
                        string s1 = item.Object.ToString();
                        if (s1.Contains("@en"))
                        {
                            d = s1.Replace("@en", "");

                            s = s + d + " ";

                        }
                        if (s1.Contains("@de"))
                        {
                            dd = s1.Replace("@de", "");

                            sd = sd + dd + " ";

                        }
                    }


                    string r = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();
                    s = s + r;
                    sd = sd + r;
                    //    g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString();

                    DateTime date = DateTime.Parse(g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10));
                    string subj;
                    try
                    {
                        subj = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();
                    }
                    catch
                    {
                        subj = "NoSubject";
                    }
                    InheritC.Inheritance_count(g.BaseUri);
                    int innum = 1;
                    if (children.GetTriplesWithSubject(g.BaseUri).Count() != 0)
                        innum = int.Parse(children.GetTriplesWithSubject(g.BaseUri).First().Object.ToString());

                    var e = new Esearch
                    {
                        Id = URI,
                        quote = s,
                        title = t,
                        description = d,
                        creator = r,
                        state = "1",
                        date = date,
                        subject = subj,
                        children = innum,
                        community = new List<string> { }
                    };


                    var ed = new Esearch
                    {
                        Id = URI,
                        quote = sd,
                        title = td,
                        description = dd,
                        creator = r,
                        state = "1",
                        date = date,
                        subject = subj,
                        children = innum,
                        community = new List<string> { }

                    };

                    ESclient.IndexDocument(e);
                    ESclient.Index(ed, i => i.Index("aimsde"));

                    foreach (Triple p in Inheritance.GetTriplesWithSubject(g.BaseUri))
                    {

                        var updateResponse = ESclient.Update<Esearch, object>(p.Object.ToString(), u => u
                             .Index("aims")
                             .Doc(new { children = Int32.Parse(children.GetTriplesWithSubject(p.Object).First().Object.ToString()) })
                             .Refresh(Refresh.WaitFor));
                        updateResponse = ESclient.Update<Esearch, object>(p.Object.ToString(), u => u
                        .Index("aimsde")
                        .Doc(new { children = Int32.Parse(children.GetTriplesWithSubject(p.Object).First().Object.ToString()) })
                        .Refresh(Refresh.WaitFor));
                    }

                    ESclient.Indices.Refresh();

                    foreach (var tt in g.GetTriplesWithPredicate(new Uri("http://www.w3.org/ns/prov#wasRevisionOf")))
                        version.Assert(tt);
                    c.ReadWriteSparqlConnector.SaveGraph(version);
                }
            }
        }


        [HttpGet("application-profiles/subclassTree")]

        public List<APModel> subclass(String query, bool includeDefinition)
        {
            List<APModel> listap = new List<APModel>();

            /*   SparqlQuery squery = sparqlparser.ParseFromString(

               @"SELECT DISTINCT ?s  WHERE {{?s ?p ?o . ?s <http://www.w3.org/2002/07/owl#imports> + <" + query + @"> . }} ");


               SparqlResultSet results = (SparqlResultSet)processorap.ProcessQuery(squery);

               var rs = results.Select(x => new Uri(x.Value("s").ToString()));*/


            foreach (Uri i in InheritC.getChildren(query))
            {

                APModel res = new();
                res.base_url = i.ToString();
                res.mimeType = "text/turtle";
                IGraph h = new Graph();
                c.ReadWriteSparqlConnector.LoadGraph(h, res.base_url);

                if (includeDefinition == true)
                {

                    res.definition = MT.TypeConvertor("Turtle", h);
                }

                foreach (var j in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")))

                {
                    string st = j.Object.ToString();
                    if (st.Contains("@en"))
                    {
                        res.description = st.Replace("@en", "");

                    }

                }
                foreach (var j in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))

                {
                    string st = j.Object.ToString();
                    if (st.Contains("@en"))
                    {
                        res.name = st.Replace("@en", "");

                    }

                }
                res.creator = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();

                res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

                // res.subject = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();

                res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());

                var projTriples = Approvals.GetTriplesWithSubject(h.BaseUri);
                var projTriples2 = underReview.GetTriplesWithSubject(h.BaseUri);

                List<string> projects = new List<string>();
                List<string> projects2 = new List<string>();

                foreach (Triple triple in projTriples)
                {

                    projects.Add(triple.Object.ToString());
                }
                foreach (Triple triple in projTriples2)
                {

                    projects2.Add(triple.Object.ToString());
                }
                res.approvedBy = projects;
                res.underReviewBy = projects2;

                List<string> child = new List<string>();

                foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
                { child.Add(triple.Subject.ToString()); }

                res.children = child;

                listap.Add(res);
            }

            return listap;

        }

        [HttpGet("application-profiles/subclass")]

        public List<APModel> sub(String query, bool includeDefinition)
        {
            List<APModel> listap = new List<APModel>();

            IEnumerable<Triple> ienum = Inheritance.GetTriplesWithObject(new Uri(query));
            foreach (var element in ienum)
            {
                APModel res = new();
                res.base_url = element.Subject.ToString();
                res.mimeType = "text/turtle";
                IGraph h = new Graph();
                c.ReadWriteSparqlConnector.LoadGraph(h, res.base_url);

                if (includeDefinition == true)
                {

                    res.definition = MT.TypeConvertor("Turtle", h);
                }

                foreach (var i in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")))

                {
                    string st = i.Object.ToString();
                    if (st.Contains("@en"))
                    {
                        res.description = st.Replace("@en", "");

                    }

                }
                foreach (var i in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))

                {
                    string st = i.Object.ToString();
                    if (st.Contains("@en"))
                    {
                        res.name = st.Replace("@en", "");

                    }

                }

                //res.description = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")).First().Object.ToString();

                res.creator = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();

                res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

                //res.subject = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();

                //en or de
                //res.name = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")).First().Object.ToString().Replace("@en", "");
                // several community problem
                res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());

                var projTriples = Approvals.GetTriplesWithSubject(h.BaseUri);
                var projTriples2 = underReview.GetTriplesWithSubject(h.BaseUri);

                List<string> projects = new List<string>();
                List<string> projects2 = new List<string>();


                foreach (Triple triple in projTriples)
                {

                    projects.Add(triple.Object.ToString());
                }
                foreach (Triple triple in projTriples2)
                {

                    projects2.Add(triple.Object.ToString());
                }
                res.approvedBy = projects;
                res.underReviewBy = projects2;

                List<string> child = new List<string>();

                foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
                { child.Add(triple.Subject.ToString()); }

                res.children = child;


                listap.Add(res);


            }


            return listap;

        }

        [HttpGet("application-profiles/superclass")]

        public List<APModel> super(String query, bool includeDefinition)
        {


            List<APModel> listap = new List<APModel>();

            IEnumerable<Triple> ienum = Inheritance.GetTriplesWithSubject(new Uri(query));
            foreach (var element in ienum)
            {
                APModel res = new();
                res.base_url = element.Object.ToString();
                res.mimeType = "text/turtle";
                IGraph h = new Graph();
                c.ReadWriteSparqlConnector.LoadGraph(h, res.base_url);

                if (includeDefinition == true)
                {

                    res.definition = MT.TypeConvertor("Turtle", h);
                }
                //    listap.Add(res);


                foreach (var i in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")))

                {
                    string st = i.Object.ToString();
                    if (st.Contains("@en"))
                    {
                        res.description = st.Replace("@en", "");

                    }

                }
                foreach (var i in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))

                {
                    string st = i.Object.ToString();
                    if (st.Contains("@en"))
                    {
                        res.name = st.Replace("@en", "");

                    }

                }

                //res.description = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")).First().Object.ToString();

                res.creator = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();

                res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

                // res.subject = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();

                //en or de
                //res.name = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")).First().Object.ToString().Replace("@en", "");
                // several community problem
                res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());

                var projTriples = Approvals.GetTriplesWithSubject(h.BaseUri);
                var projTriples2 = underReview.GetTriplesWithSubject(h.BaseUri);

                List<string> projects = new List<string>();
                List<string> projects2 = new List<string>();

                foreach (Triple triple in projTriples)
                {

                    projects.Add(triple.Object.ToString());
                }
                foreach (Triple triple in projTriples2)
                {

                    projects2.Add(triple.Object.ToString());
                }
                res.approvedBy = projects;
                res.underReviewBy = projects2;

                List<string> child = new List<string>();

                foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
                { child.Add(triple.Subject.ToString()); }

                res.children = child;

                listap.Add(res);

            }

            return listap;
        }

        [HttpGet("application-profiles/{uri}")]
        public APModel AP(String uri, String accessToken, Boolean includeDefinition)
        {

            var d = System.Net.WebUtility.UrlDecode(uri);
            APModel res = new();

            if (states.GetTriplesWithSubject(UriFactory.Create(d)).First().Object.ToString() != "0")
            {

                IGraph h = new Graph();
                c.ReadWriteSparqlConnector.LoadGraph(h, new Uri(d));
                if (includeDefinition == true)
                {

                    res.definition = MT.TypeConvertor("text/turtle", h);

                }


                foreach (var item in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")))

                {
                    string s1 = item.Object.ToString();
                    if (s1.Contains("@en"))
                    {
                        res.description = s1.Replace("@en", "");

                    }
                    /*
                    else if (s1.Contains("@de"))
                    {
                        res.description = s1.Replace("@de", "");

                    }*/
                }
                //    res.description = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")).First().Object.ToString();

                foreach (var item in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))

                {
                    string s1 = item.Object.ToString();
                    if (s1.Contains("@en"))
                    {
                        res.name = s1.Replace("@en", "");

                    }
                    /*
                    else if (s1.Contains("@de"))
                    {
                        res.name = s1.Replace("@de", "");

                    }*/
                }
                //  res.name = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")).First().Object.ToString().Replace("@en", "");

                res.creator = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();

                res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

                //res.subject = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();

                res.base_url = h.BaseUri.ToString();

                res.mimeType = "text/turtle";
                //en or de
                // several community problem
                res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());
                var Triples = Approvals.GetTriplesWithSubject(h.BaseUri);
                var Triples2 = underReview.GetTriplesWithSubject(h.BaseUri);

                List<string> projects = new List<string>();
                List<string> projects2 = new List<string>();

                foreach (Triple t in Triples)
                {

                    projects.Add(t.Object.ToString());
                }
                foreach (Triple t in Triples2)
                {

                    projects2.Add(t.Object.ToString());
                }
                // projects.Add(h.GetTriplesWithPredicate(new Uri("http://schema.org/Project")).First().Object.ToString());

                res.approvedBy = projects;
                res.underReviewBy = projects2;

                List<string> child = new List<string>();

                foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
                { child.Add(triple.Subject.ToString()); }

                res.children = child;
                res.versions = versionTree(d);
            }
            else
            {
                if (uuid.GetTriplesWithSubject(new Uri(d)).First().Object.ToString() == accessToken)
                {
                    IGraph h = new Graph();
                    c.ReadWriteSparqlConnector.LoadGraph(h, new Uri(d));
                    if (includeDefinition == true)
                    {

                        res.definition = MT.TypeConvertor("text/turtle", h);

                    }

                    foreach (var item in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")))

                    {
                        string s1 = item.Object.ToString();
                        if (s1.Contains("@en"))
                        {
                            res.description = s1.Replace("@en", "");

                        }
                        /*
                        else if (s1.Contains("@de"))
                        {
                            res.description = s1.Replace("@de", "");

                        }*/
                    }
                    //    res.description = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")).First().Object.ToString();

                    foreach (var item in h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))

                    {
                        string s1 = item.Object.ToString();
                        if (s1.Contains("@en"))
                        {
                            res.name = s1.Replace("@en", "");

                        }
                        /*
                        else if (s1.Contains("@de"))
                        {
                            res.name = s1.Replace("@de", "");

                        }*/
                    }
                    //    res.description = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")).First().Object.ToString();
                    //    res.name = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")).First().Object.ToString().Replace("@en", "");

                    res.creator = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();

                    res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

                    // res.subject = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();

                    res.base_url = h.BaseUri.ToString();

                    res.mimeType = "text/turtle";
                    //en or de
                    // several community problem
                    res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());

                    var Triples = Approvals.GetTriplesWithSubject(h.BaseUri);
                    var Triples2 = underReview.GetTriplesWithSubject(h.BaseUri);

                    List<string> projects = new List<string>();
                    List<string> projects2 = new List<string>();

                    foreach (Triple t in Triples)
                    {
                        projects.Add(t.Object.ToString());
                    }
                    foreach (Triple t in Triples2)
                    {
                        projects2.Add(t.Object.ToString());
                    }
                    res.approvedBy = projects;
                    res.underReviewBy = projects2;

                    List<string> child = new List<string>();

                    foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
                    { child.Add(triple.Subject.ToString()); }

                    res.children = child;
                    res.versions = versionTree(d);

                }

            }
            return res;
        }

        [HttpGet("application-profiles/RawRDF/{uri}")]

        public ContentResult GetAP(String uri)
        {

            var d = System.Net.WebUtility.UrlDecode(uri);
            //String accept=Request.Headers["accept"];
            //if (accept == null) 
            //{ accept = "text/turtle"; }
            if (states.GetTriplesWithSubject(UriFactory.Create(d)).First().Object.ToString() != "0")
            {
                IGraph h = new Graph();
                c.ReadWriteSparqlConnector.LoadGraph(h, new Uri(d));

                return new ContentResult
                {
                    Content = MT.TypeConvertor(Request.Headers["accept"], h),
                    ContentType = Request.Headers["accept"],

                };
            }
            else
                return new ContentResult
                {
                    Content = "Private AP!",
                    ContentType = Request.Headers["accept"],

                };
        }

        [HttpGet("application-profiles/XML/{uri}")]

        public ActionResult getXML(String uri)
        {
            var d = System.Net.WebUtility.UrlDecode(uri);
            if (states.GetTriplesWithSubject(UriFactory.Create(d)).First().Object.ToString() != "0")
            {
                IGraph h = new Graph();

                c.ReadWriteSparqlConnector.LoadGraph(h, new Uri(d));

                //  return MT.TypeConvertor("text/xml", h);
                return Content(MT.TypeConvertor("text/xml", h), "text/xml", System.Text.Encoding.UTF8);

                //  return Content(res.definition, "text/xml", System.Text.Encoding.UTF8);
            }
            else
                return Content("Private AP!", "text/xml", System.Text.Encoding.UTF8);
        }



        [HttpGet("application-profiles/versionTree")]

        public List<string> versionTree(String Uri)
        {
            InMemoryDataset dsap;
            ISparqlQueryProcessor processorap;
            SparqlQueryParser sparqlparser = new SparqlQueryParser();
            dsap = new InMemoryDataset(version);
            processorap = new LeviathanQueryProcessor(dsap);

            SparqlQuery squery = sparqlparser.ParseFromString(

            @"SELECT DISTINCT ?s  WHERE {{?s ?p ?o . ?s <http://www.w3.org/ns/prov#wasRevisionOf> + <" + Uri + @"> . }} ");

            SparqlResultSet results = (SparqlResultSet)processorap.ProcessQuery(squery);

            //  results.Select(x => new Uri(x.Value("s").ToString()));

            List<string> v = new List<string>();
            foreach (Uri i in results.Select(x => new Uri(x.Value("s").ToString())))
            { v.Add(i.ToString()); }
            v.Sort();
            return v;
            /*
            List<string> v = new List<string>();
            foreach (Triple triple in version.GetTriplesWithObject(new Uri(Uri)))
            { v.Add(triple.Subject.ToString()); }
            v.Sort();
            return v;*/
        }

        [HttpGet("application-profiles/versions")]

        public List<string> versions(String Uri)
        {

            List<string> v = new List<string>();
            foreach (Triple triple in version.GetTriplesWithObject(new Uri(Uri)))
            { v.Add(triple.Subject.ToString()); }
            v.Sort();
            return v;
        }

        [HttpGet("application-profiles")]
        //autocomplete
        public List<APModel> GetAP(String query, String description, String creator, string language, DateTime? from, DateTime? to, string subject, string community, string state, bool includeDefinition)
        {

            List<APModel> listap = new List<APModel>();
            /*   if (state.IsNullOrEmpty() == true)
               {
                   state = "public";
               }*/
            if (from == null)
            {
                from = DateTime.MinValue;
            }
            if (to == null)
            {
                to = DateTime.MaxValue;
            }

            List<string> optionList = new List<string> { "state0", "public", "state2", "approved" };

            var profiles = ESClass.autocomplete(query, description, creator, language, from, to, subject, community, optionList.IndexOf(state), includeDefinition).Documents;

            foreach (Esearch i in profiles)
            {

                APModel res = new();
                res.mimeType = "text/turtle";
                res.base_url = i.Id;
                res.name = i.title;

                IGraph h = new Graph();
                c.ReadWriteSparqlConnector.LoadGraph(h, new Uri(i.Id));

                if (includeDefinition == true)
                {

                    res.definition = MT.TypeConvertor("Turtle", h);
                }
                res.description = i.description;

                res.creator = i.creator;
                res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());

                res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

                res.subject = i.subject;

                var projTriples = Approvals.GetTriplesWithSubject(h.BaseUri);
                var projTriples2 = underReview.GetTriplesWithSubject(h.BaseUri);


                List<string> projects = new List<string>();
                List<string> projects2 = new List<string>();

                foreach (Triple triple in projTriples)
                {

                    projects.Add(triple.Object.ToString());
                }
                foreach (Triple triple in projTriples2)
                {

                    projects2.Add(triple.Object.ToString());
                }
                res.approvedBy = projects;
                res.underReviewBy = projects2;

                List<string> child = new List<string>();

                foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
                { child.Add(triple.Subject.ToString()); }

                res.children = child;

                res.versions = versionTree(i.Id);

                listap.Add(res);
            }

            return listap;


        }


        /*
        [HttpGet("application-profiles/FullTextSearch")]

        public List<APModel> FullTextSearch(String query, string language, string state, bool includeDefinition)
        {

            List<APModel> listap = new List<APModel>();
            List<string> optionList = new List<string> { "state0", "public", "state2", "approved" };

            var profiles = ESClass.FuzzyFullText(query, language, optionList.IndexOf(state).ToString(), includeDefinition).Documents;

            foreach (Esearch i in profiles)
            {

                APModel res = new();

                res.mimeType = "text/turtle";
                res.base_url = i.Id;
                res.name = i.title;

                IGraph h = new Graph();
                c.ReadWriteSparqlConnector.LoadGraph(h, new Uri(i.Id));

                if (includeDefinition == true)
                {

                    res.definition = MT.TypeConvertor("Turtle", h);
                }
                res.description = i.description;

                res.creator = i.creator;
                res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());

                res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

                //res.subject = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();

                var projTriples = Approvals.GetTriplesWithSubject(h.BaseUri);
                var projTriples2 = underReview.GetTriplesWithSubject(h.BaseUri);

                List<string> projects = new List<string>();
                List<string> projects2 = new List<string>();

                foreach (Triple triple in projTriples)
                {

                    projects.Add(triple.Object.ToString());
                }
                foreach (Triple triple in projTriples2)
                {

                    projects2.Add(triple.Object.ToString());
                }
                res.approvedBy = projects;
                res.underReviewBy = projects2;


                List<string> child = new List<string>();

                foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
                { child.Add(triple.Subject.ToString()); }

                res.children = child;

                listap.Add(res);
            }

            //List<APModel> objListOrder = listap.OrderBy(order => order.numInherit).ToList();
            return listap;

        }
        */
        [HttpGet("application-profiles/multiMatch")]

        public List<APModel> multiMatch(String title, String description, String creator, string language, DateTime? from, DateTime? to, string subject, string community, string state, bool includeDefinition)
        {

            List<APModel> listap = new List<APModel>();
            if (state.IsNullOrEmpty() == true)
            {
                state = "public";
            }
            if (from == null)
            {
                from = DateTime.MinValue;
            }
            if (to == null)
            {
                to = DateTime.MaxValue;
            }

            List<string> optionList = new List<string> { "state0", "public", "state2", "approved" };

            var profiles = ESClass.multifield(title, description, creator, language, from, to, subject, community, optionList.IndexOf(state), includeDefinition).Documents;

            foreach (Esearch i in profiles)
            {

                APModel res = new();
                res.mimeType = "text/turtle";
                res.base_url = i.Id;
                res.name = i.title;

                IGraph h = new Graph();
                c.ReadWriteSparqlConnector.LoadGraph(h, new Uri(i.Id));

                if (includeDefinition == true)
                {

                    res.definition = MT.TypeConvertor("Turtle", h);
                }
                res.description = i.description;

                res.creator = i.creator;
                res.state = Int32.Parse(states.GetTriplesWithSubject(h.BaseUri).First().Object.ToString());

                res.created = h.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10);

                res.subject = i.subject;

                var projTriples = Approvals.GetTriplesWithSubject(h.BaseUri);
                var projTriples2 = underReview.GetTriplesWithSubject(h.BaseUri);


                List<string> projects = new List<string>();
                List<string> projects2 = new List<string>();

                foreach (Triple triple in projTriples)
                {

                    projects.Add(triple.Object.ToString());
                }
                foreach (Triple triple in projTriples2)
                {

                    projects2.Add(triple.Object.ToString());
                }
                res.approvedBy = projects;
                res.underReviewBy = projects2;

                List<string> child = new List<string>();

                foreach (Triple triple in Inheritance.GetTriplesWithObject(h.BaseUri))
                { child.Add(triple.Subject.ToString()); }

                res.children = child;

                res.versions = versionTree(i.Id);

                listap.Add(res);
            }

            return listap;

        }

        
        [HttpPut("application-profiles/Import")]
        [Consumes("application/json")]

        public void Import([FromBody] APModel ap)
        {

            IGraph g = new Graph();
            String URI = g.GetTriplesWithObject(new Uri("http://www.w3.org/ns/shacl#NodeShape")).First().Subject.ToString();
            ap.mimeType = "text/turtle";

            IRdfReader reader = MimeTypesHelper.GetParser(ap.mimeType);

            StringParser.Parse(g, ap.definition.ToString(), reader);

            g.BaseUri = new Uri(URI);

            //if (APShape.Validate(g).Conforms == true)

            string s = "";
            string t = "";

            string sd = "";
            string td = "";

            foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))
            {
                string s1 = item.Object.ToString();
                if (s1.Contains("@en"))
                {
                    t = s1.Replace("@en", "");

                    s = s + t + " ";
                }
                if (s1.Contains("@de"))
                {
                    td = s1.Replace("@de", "");

                    sd = sd + td + " ";
                }
            }


            string d = "";
            string dd = "";

            foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description"))
)
            {
                string s1 = item.Object.ToString();
                if (s1.Contains("@en"))
                {
                    d = s1.Replace("@en", "");

                    s = s + d + " ";

                }
                if (s1.Contains("@de"))
                {
                    dd = s1.Replace("@de", "");

                    sd = sd + dd + " ";

                }
            }



            string r = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();
            s = s + r;

            //   g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString();

            DateTime date = DateTime.Parse(g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10));
            string subj;
            try
            {
                subj = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();
            }
            catch
            {
                subj = "NoSubject";
            }

            c.ReadWriteSparqlConnector.SaveGraph(g);

            //    APs.Merge(g);
            //    c.ReadWriteSparqlConnector.SaveGraph(APs);

            InheritC.Inheritance_count(g.BaseUri);

            int innum = 1;
            if (children.GetTriplesWithSubject(g.BaseUri).Count() != 0)
                innum = int.Parse(children.GetTriplesWithSubject(g.BaseUri).First().Object.ToString());

            var e = new Esearch
            {
                Id = URI,
                quote = s,
                title = t,
                description = d,
                creator = r,
                state = "1",

                date = date,
                subject = subj,
                children = innum,
                community = new List<string> { }

            };

            var ed = new Esearch
            {
                Id = URI,
                quote = sd,
                title = td,
                description = dd,
                creator = r,
                state = "1",
                date = date,
                subject = subj,
                children = innum,
                community = new List<string> { }

            };

            ESclient.IndexDocument(e);
            ESclient.Index(ed, i => i.Index("aimsde"));

            foreach (Triple p in Inheritance.GetTriplesWithSubject(g.BaseUri))
            {

                var updateResponse = ESclient.Update<Esearch, object>(p.Object.ToString(), u => u
                     .Index("aims")
                     .Doc(new { children = Int32.Parse(children.GetTriplesWithSubject(p.Object).First().Object.ToString()) })
                     .Refresh(Refresh.WaitFor));
                updateResponse = ESclient.Update<Esearch, object>(p.Object.ToString(), u => u
                .Index("aimsde")
                .Doc(new { children = Int32.Parse(children.GetTriplesWithSubject(p.Object).First().Object.ToString()) })
                .Refresh(Refresh.WaitFor));
            }


            ESclient.Indices.Refresh();


            Graph st = new Graph();
            IUriNode su = st.CreateUriNode(UriFactory.Create(URI));
            IUriNode pre = st.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/type"));
            ILiteralNode ob = st.CreateLiteralNode("1");
            st.Assert(new Triple(su, pre, ob));
            states.Merge(st);
            c.ReadWriteSparqlConnector.SaveGraph(states);

            foreach (var tt in g.GetTriplesWithPredicate(new Uri("http://www.w3.org/ns/prov#wasRevisionOf")))
                version.Assert(tt);
            c.ReadWriteSparqlConnector.SaveGraph(version);


        }

        [HttpPut("application-profiles/GitSync")]
        [Consumes("application/json")]

        public void sync([FromBody] APModel ap)
        {

            String URI = ap.base_url;

            IGraph g = new Graph();
            IRdfReader reader = MimeTypesHelper.GetParser(ap.mimeType);

            StringParser.Parse(g, ap.definition.ToString(), reader);

            g.BaseUri = new Uri(URI);

            //if (APShape.Validate(g).Conforms == true)

            string s = "";
            string t = "";

            string sd = "";
            string td = "";

            foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))
            {
                string s1 = item.Object.ToString();
                if (s1.Contains("@en"))
                {
                    t = s1.Replace("@en", "");

                    s = s + t + " ";
                }
                if (s1.Contains("@de"))
                {
                    td = s1.Replace("@de", "");

                    sd = sd + td + " ";
                }
            }


            string d = "";
            string dd = "";

            foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description"))
)
            {
                string s1 = item.Object.ToString();
                if (s1.Contains("@en"))
                {
                    d = s1.Replace("@en", "");

                    s = s + d + " ";

                }
                if (s1.Contains("@de"))
                {
                    dd = s1.Replace("@de", "");

                    sd = sd + dd + " ";

                }
            }



            string r = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();
            s = s + r;

            //   g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString();

            DateTime date = DateTime.Parse(g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString().Substring(0, 10));
            string subj;
            try
            {
                subj = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/subject")).First().Object.ToString();
            }
            catch
            {
                subj = "NoSubject";
            }

            c.ReadWriteSparqlConnector.SaveGraph(g);

            //    APs.Merge(g);
            //    c.ReadWriteSparqlConnector.SaveGraph(APs);

            InheritC.Inheritance_count(g.BaseUri);

            int innum = 1;
            if (children.GetTriplesWithSubject(g.BaseUri).Count() != 0)
                innum = int.Parse(children.GetTriplesWithSubject(g.BaseUri).First().Object.ToString());

            var e = new Esearch
            {
                Id = URI,
                quote = s,
                title = t,
                description = d,
                creator = r,
                state = "1",

                date = date,
                subject = subj,
                children = innum,
                community = new List<string> { }

            };

            var ed = new Esearch
            {
                Id = URI,
                quote = sd,
                title = td,
                description = dd,
                creator = r,
                state = "1",
                date = date,
                subject = subj,
                children = innum,
                community = new List<string> { }

            };

            ESclient.IndexDocument(e);
            ESclient.Index(ed, i => i.Index("aimsde"));

            foreach (Triple p in Inheritance.GetTriplesWithSubject(g.BaseUri))
            {

                var updateResponse = ESclient.Update<Esearch, object>(p.Object.ToString(), u => u
                     .Index("aims")
                     .Doc(new { children = Int32.Parse(children.GetTriplesWithSubject(p.Object).First().Object.ToString()) })
                     .Refresh(Refresh.WaitFor));
                updateResponse = ESclient.Update<Esearch, object>(p.Object.ToString(), u => u
                .Index("aimsde")
                .Doc(new { children = Int32.Parse(children.GetTriplesWithSubject(p.Object).First().Object.ToString()) })
                .Refresh(Refresh.WaitFor));
            }


            ESclient.Indices.Refresh();


            Graph st = new Graph();
            IUriNode su = st.CreateUriNode(UriFactory.Create(URI));
            IUriNode pre = st.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/type"));
            ILiteralNode ob = st.CreateLiteralNode("1");
            st.Assert(new Triple(su, pre, ob));
            states.Merge(st);
            c.ReadWriteSparqlConnector.SaveGraph(states);

            foreach (var tt in g.GetTriplesWithPredicate(new Uri("http://www.w3.org/ns/prov#wasRevisionOf")))
                version.Assert(tt);
            c.ReadWriteSparqlConnector.SaveGraph(version);


        }

        [HttpGet("application-profiles/SubmitToCommunity")]

        public async Task<IActionResult> GitLabMergeRequest(String uri, String community)
        {
            // if (states.GetTriplesWithSubject(UriFactory.Create(uri)).First().Object.ToString() == "1")
            // { 
            //if (uuid.GetTriplesWithSubject(new Uri(APuri)).First().Object.ToString() == id)
            // {
            String APurl = "https://profiles.nfdi4ing.de/#/editor?applicationProfile=" + System.Net.WebUtility.UrlEncode(uri);

            IGraph h = new Graph();
            c.ReadWriteSparqlConnector.LoadGraph(h, uri);
            String definition = MT.TypeConvertor("Turtle", h);

            var fileName = uri.Replace("https://w3id.org/nfdi4ing/profiles/", "");
            fileName = fileName[..(fileName.Length)];
            //Hide token 

            var gitlabURL = config.GetValue("AIMS_GITLAB_URL", "https://git.rwth-aachen.de/");
            var gitlabPassword = config.GetValue<string>("AIMS_GITLAB_PASSWORD");
            var client = new GitLabClient(gitlabURL, gitlabPassword);

            var newBranchName = "Request/" + Guid.NewGuid().ToString();
            await client.Branches.CreateAsync("metadata-profile-curation/" + community, new CreateBranchRequest(newBranchName, "main"));
            var actions = new List<CreateCommitRequestAction>
                {
                    new CreateCommitRequestAction(
                        new CreateCommitRequestActionType(),
                        $"Profiles/"+ fileName + ".ttl") {
                    //        community+ fileName + ".ttl") {
                            Content = definition
                        }
                };

            await client.Commits.CreateAsync("metadata-profile-curation/" + community, new CreateCommitRequest(newBranchName, APurl, actions), false);

            var newMergeRequest = await client.MergeRequests.CreateAsync("metadata-profile-curation/" + community,
                new CreateMergeRequest(newBranchName, "main", $"Merge request for application profile : {APurl}")
                {

                    Labels = new[] { community }
                });

            if (states.GetTriplesWithSubject(UriFactory.Create(uri)).First().Equals("3") == false)
            {
                states.Retract(states.GetTriplesWithSubject(UriFactory.Create(uri)).First());
                c.ReadWriteSparqlConnector.SaveGraph(states);
                Graph st = new Graph();
                st.Assert(new Triple(st.CreateUriNode(UriFactory.Create(uri)), st.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/type")), st.CreateLiteralNode("2")));
                states.Merge(st);
                c.ReadWriteSparqlConnector.SaveGraph(states);
            }

            Graph st2 = new Graph();
            IUriNode su = st2.CreateUriNode(UriFactory.Create(uri));
            IUriNode pre = st2.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/identifier"));
            ILiteralNode ob = st2.CreateLiteralNode(newMergeRequest.Iid.ToString());
            st2.Assert(new Triple(su, pre, ob));
            MergRequestID.Merge(st2);
            c.ReadWriteSparqlConnector.SaveGraph(MergRequestID);

            Graph st3 = new Graph();
            su = st3.CreateUriNode(UriFactory.Create(uri));
            pre = st3.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/review"));
            ob = st3.CreateLiteralNode(community);
            st3.Assert(new Triple(su, pre, ob));
            underReview.Merge(st3);
            c.ReadWriteSparqlConnector.SaveGraph(underReview);
            
            return Content(newMergeRequest.WebUrl.ToString());
            
        }




        [HttpGet("Application-Profiles/approve")]

        public string approve(string community)
        {
            var client = new GitLabClient(config.GetValue<string>("AIMS_GITLAB_URL"), config.GetValue<string>("AIMS_GITLAB_PASSWORD"));
            string state = "";
            foreach (var item in MergRequestID.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/identifier")))
            {
                state = client.MergeRequests.GetAsync("metadata-profile-curation/" + community, Int32.Parse(item.Object.ToString())).Result.State.ToString();
                if (state == "Closed")
                {
                    MergRequestID.Retract(item);
                    c.ReadWriteSparqlConnector.SaveGraph(MergRequestID);

                    if (states.GetTriplesWithSubject(item.Subject).Equals("3") == false)
                    {
                        states.Retract(states.GetTriplesWithSubject(item.Subject));
                        c.ReadWriteSparqlConnector.SaveGraph(states);
                        Graph st = new Graph();
                        //    IUriNode su = (IUriNode)item.Subject;
                        IUriNode pre = st.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/type"));
                        ILiteralNode ob = st.CreateLiteralNode("4");
                        st.Assert(new Triple(Tools.CopyNode(item.Subject, st), pre, ob));
                        states.Merge(st);
                        c.ReadWriteSparqlConnector.SaveGraph(states);
                    }
                    Graph st4 = new Graph();
                    //  pre = st4.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/review"));

                    //  st4.Assert(new Triple(Tools.CopyNode(item.Subject, st4), pre, ob));
                    underReview.Retract(underReview.GetTriplesWithSubjectObject(item.Subject, st4.CreateLiteralNode(community)));
                    c.ReadWriteSparqlConnector.SaveGraph(underReview);

                    /*
                    underReview.Retract(states.GetTriplesWithSubject(new Triple(Tools.CopyNode(item.Subject, st), pre, ob)));
                    c.ReadWriteSparqlConnector.SaveGraph(underReview);*/
                }
                else if (state == "Merged")
                {
                    MergRequestID.Retract(item);
                    c.ReadWriteSparqlConnector.SaveGraph(MergRequestID);

                    states.Retract(states.GetTriplesWithSubject(item.Subject));
                    c.ReadWriteSparqlConnector.SaveGraph(states);
                    Graph st = new Graph();
                    //  IUriNode su =item.Subject;
                    IUriNode pre = st.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/type"));
                    ILiteralNode ob = st.CreateLiteralNode("3");
                    st.Assert(new Triple(Tools.CopyNode(item.Subject, st), pre, ob));
                    states.Merge(st);
                    c.ReadWriteSparqlConnector.SaveGraph(states);

                    //    string community = client.MergeRequests.GetAsync("metadata-profile-curation/nfdi4ing", Int32.Parse(item.Object.ToString())).Result.Labels.First();

                    Graph st2 = new Graph();
                    pre = st2.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/approvedBy"));
                    ob = st2.CreateLiteralNode(community);
                    st2.Assert(new Triple(Tools.CopyNode(item.Subject, st2), pre, ob));
                    Approvals.Merge(st2);
                    c.ReadWriteSparqlConnector.SaveGraph(Approvals);

                    Graph st4 = new Graph();
                    //  pre = st4.CreateUriNode(UriFactory.Create("http://purl.org/dc/terms/review"));
                    ob = st4.CreateLiteralNode(community);
                    //  st4.Assert(new Triple(Tools.CopyNode(item.Subject, st4), pre, ob));
                    underReview.Retract(underReview.GetTriplesWithSubjectObject(item.Subject, ob));
                    c.ReadWriteSparqlConnector.SaveGraph(underReview);

                    /*
                    IGraph h = new Graph();
                    c.ReadWriteSparqlConnector.LoadGraph(h, item.Subject.ToString());
                    foreach (var t in h.GetTriplesWithPredicate(new Uri("http://www.w3.org/2002/07/owl#imports")))
                        children.Assert(t);   
                    c.ReadWriteSparqlConnector.SaveGraph(children);*/

                    /*
                    IGraph h = new Graph();
                    c.ReadWriteSparqlConnector.LoadGraph(h, item.Subject.ToString());
                    h.Assert(new Triple(h.CreateUriNode(UriFactory.Create(item.Subject.ToString())), 
                    h.CreateUriNode(UriFactory.Create("http://schema.org/Project")), 
                    h.CreateLiteralNode(community)));
                    c.ReadWriteSparqlConnector.SaveGraph(h);*/

                    var updateResponse = ESclient.Update<Esearch, object>(item.Subject.ToString(), u => u
                     .Index("aims")
                     .Doc(new { state = "3" })
                     .Refresh(Refresh.WaitFor));
                    updateResponse = ESclient.Update<Esearch, object>(item.Subject.ToString(), u => u
                    .Index("aimsde")
                    .Doc(new { state = "3" })
                    .Refresh(Refresh.WaitFor));

                    var doc = ESclient.Get<Esearch>(item.Subject.ToString(), g => g.Index("aims"));
                    var listc = doc.Source.community;
                    listc.Add(community);
                    updateResponse = ESclient.Update<Esearch, object>(item.Subject.ToString(), u => u
                             .Index("aims")
                             .Doc(new { community = listc })
                             .Refresh(Refresh.WaitFor));

                    doc = ESclient.Get<Esearch>(item.Subject.ToString(), g => g.Index("aimsde"));
                    listc = doc.Source.community;
                    listc.Add(community);
                    updateResponse = ESclient.Update<Esearch, object>(item.Subject.ToString(), u => u
                             .Index("aimsde")
                             .Doc(new { community = listc })
                             .Refresh(Refresh.WaitFor));

                  
                }

            }
            return state;
        }

        [HttpGet("Communities")]
        public async Task<List<Community>> Communities()
        {

            string gitLabUrl = config.GetValue<string>("AIMS_GITLAB_URL") + "api/v4/";
            string groupId = config.GetValue<string>("AIMS_GITLAB_GROUPID");
            string accessToken = config.GetValue<string>("AIMS_GITLAB_PASSWORD");
            HttpClient client = new HttpClient();

            client.BaseAddress = new Uri(gitLabUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            HttpResponseMessage response = await client.GetAsync($"groups/{groupId}/projects");

            return await response.Content.ReadAsAsync<List<Community>>();
        }



        [HttpGet("oai2")]
        [HttpPost("oai2")]
        public ActionResult verbs(String verb, String? identifier = null, String? metadataPrefix = null, DateTime? from = null, DateTime? until = null)
        {
            oai oaiInstance = new oai(c, states);
            String tree = "";
            if (from == null)
            {
                from = DateTime.MinValue;
            }
            if (until == null)
            {
                until = DateTime.MaxValue;
            }
            if (verb.ToLower() == "identify")
                tree = oaiInstance.identify();
            else if (verb.ToLower() == "getrecord")
                tree = oaiInstance.GetRecord(identifier, metadataPrefix);
            else if (verb.ToLower() == "listidentifiers")
                tree = oaiInstance.ListIdentifiers(metadataPrefix);
            else if (verb.ToLower() == "listrecords")
                tree = oaiInstance.ListRecords(metadataPrefix, from, until);
            else if (verb.ToLower() == "listmetadataformats")
                tree = oaiInstance.ListMetadataFormats();
            return Content(tree, "text/xml", System.Text.Encoding.UTF8);
        }

    }

}
public class APModel
{
    public String name { get; set; }
    public String base_url { get; set; }
    public String definition { get; set; }
    public String mimeType { get; set; }
    //   public int numInherit { get; set; }
    public int state { get; set; }
    public string accessToken { get; set; }
    public string description { get; set; }
    public string creator { get; set; }
    public string created { get; set; }
    public string subject { get; set; }
    public List<string> approvedBy { get; set; }
    public List<string> underReviewBy { get; set; }
    public List<string> children { get; set; }
    public List<string> versions { get; set; }
}

public class TermModel
{
    public Boolean property { get; set; }
    public String name { get; set; }
    public String uri { get; set; }
    public String label { get; set; }
    public String definition { get; set; }
    public String mimeType { get; set; }


}

public class Esearch
{

    public string quote { get; set; }
    public string Id { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string creator { get; set; }
    public string created { get; set; }
    public string state { get; set; }
    public int children { get; set; }
    public string subject { get; set; }
    public DateTime date { get; set; }
    public List<string> community { get; set; }

}



public class Community
{
    public int Id { get; set; }
    public string name { get; set; }
    public string Path { get; set; }

}


