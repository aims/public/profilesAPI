# API
https://pg4aims.ulb.tu-darmstadt.de/swagger/index.html
# Backend components

# Features

1. Virtuoso RDF triple store (Open Source Edition) for storing metadata standards and all the data of AIMS as a web API

1. Supporting inheritance relations between metadata standards and retrieving sub classes and super classes 

1. Validating metadata standards according to the AIMS’s metadata standard. Each metadata standard must have metadata such as title, description and creator.

1. Using Elasticsearch for searching the metadata standards 


1. Submitting metadata standards from AIMS to NFDi4ing communities in Gitlab for review, discussion and approval by making merge requests

1. Tagging the metadata standards that are approved by NFDi4ing communities

1. Exposing the metadata standards according to Open Archives Initiative Protocol for Metadata Harvesting (OAI-PMH)





### Open Archives Initiative Protocol for Metadata Harvesting (OAI-PMH)

AIMS exposes the metadata standards according to Open Archives Initiative Protocol for Metadata Harvesting (OAI-PMH). This protocol provides an application-independent interoperability framework based on metadata harvesting. There are two classes of participants in the OAI-PMH framework, data providers and service providers. AIMS as a data provider supports the OAI-PMH as a means of exposing metadata of metadata standards for service providers that use metadata harvested via the OAI-PMH as a basis for building value-added services. 

- https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2?verb=ListRecords

- https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2?verb=identify

- https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2?verb=listidentifiers

 

