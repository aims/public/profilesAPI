﻿using AIMS;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Query;

namespace AIMSapi
{
    public class Terms
    {

        
       
        SparqlQueryParser parser;
        ISparqlQueryProcessor processor;
        IGraph vocabulary;
        VirtuosoConnector vc;
        MimeType MT = new();
        public Terms (SparqlQueryParser p, ISparqlQueryProcessor pro,IGraph vocab,VirtuosoConnector c)
        {
            parser=p;
            processor=pro;
            vocabulary=vocab;
            vc=c;
        }


        //        [HttpPut("Terms/Put")]

        public void AddTerm([FromBody] object term)
        {

            Graph t = new Graph();
            t.LoadFromString(term.ToString());
            vocabulary.Merge(t);
            vc.ReadWriteSparqlConnector.SaveGraph(vocabulary);
        }



 //       [HttpPut("Terms")]
 //       [Consumes("application/json")]

        public void AddTerms([FromBody] TermModel terms)
        {


            IGraph g = new Graph();
            IRdfReader reader = MimeTypesHelper.GetParser(terms.mimeType);
            StringParser.Parse(g, terms.definition.ToString(), reader);
            vocabulary.Merge(g);
            vc.ReadWriteSparqlConnector.SaveGraph(vocabulary);
        }



 //       [HttpPut("Terms/ReadFile")]

        public void readTerm()
        {

            Graph t = new Graph();
            FileLoader.Load(t, @"/home/homa/Terms.ttl");
            vocabulary.Merge(t);

            vc.ReadWriteSparqlConnector.SaveGraph(vocabulary);

        }
//        [HttpGet("Terms/Get")]
        public TermModel GetTerm(String URI, String mimetype)
        {

            Graph r = new Graph();

            r.Assert(vocabulary.GetTriplesWithSubject(new Uri(URI)));

            TermModel res = new();
            res.uri = URI;
            res.definition = MT.TypeConvertor(mimetype, r); ;
            return res;

        }
//        [HttpGet("Terms/GetAll")]
        public TermModel allTerms(String mimetype)
        {


            TermModel res = new();
            res.uri = "http://www.example.org/AIMS/Vocabulary/";
            res.definition = MT.TypeConvertor(mimetype, vocabulary);
            return res;
        }

//        [HttpDelete("Terms/Delete")]
        public void DeleteTerm(String URI)
        {

            vocabulary.Retract(vocabulary.GetTriplesWithSubject(new Uri(URI)));
            vc.ReadWriteSparqlConnector.SaveGraph(vocabulary);
        }



//        [HttpGet("Terms/Search")]
        public List<TermModel> search(String query, bool includeDefinition)
        {
            List<TermModel> Terms = new List<TermModel>();

            SparqlQuery squery = parser.ParseFromString(

            @"SELECT DISTINCT ?s  WHERE {?s ?p ?o . FILTER(contains(str(?s), " + @"""" + query + @"""" + @"))}");

            SparqlResultSet results = (SparqlResultSet)processor.ProcessQuery(squery);

            var rs = results.Select(x => new Uri(x.Value("s").ToString()));


            foreach (Uri i in rs)
            {

                TermModel res = new();

                res.mimeType = "text/turtle";
                res.uri = i.ToString();

                SparqlQuery q = parser.ParseFromString(

                @"SELECT  ?o  WHERE {<" + res.uri + "> <http://www.w3.org/2000/01/rdf-schema#label> ?o . }");
                SparqlResultSet results2 = (SparqlResultSet)processor.ProcessQuery(q);

                var rlabel = results2.Select(x => x.Value("o").ToString());

                try
                {

                    res.label = rlabel.ElementAt(0);
                }

                catch
                {
                    res.label = "";
                }

                if (includeDefinition == true)
                {
                    IGraph h = new Graph();
                    h.Assert(vocabulary.GetTriplesWithSubject(i));

                    res.definition = MT.TypeConvertor("Turtle", h);
                }

                Terms.Add(res);
            }

            return Terms;
        }
    }
}
