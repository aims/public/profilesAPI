﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDS.RDF.Query;
using VDS.RDF.Update;
using VDS.RDF.Storage;



namespace AIMS
{
    public class VirtuosoConnector
    {
        public readonly SparqlRemoteUpdateEndpoint UpdateEndpoint;
        public readonly SparqlRemoteEndpoint QueryEndpoint;
        public readonly ReadWriteSparqlConnector ReadWriteSparqlConnector;

        public VirtuosoConnector(string sparqlEndpoint)
        {
            UpdateEndpoint = new SparqlRemoteUpdateEndpoint(new Uri(sparqlEndpoint));
            QueryEndpoint = new SparqlRemoteEndpoint(new Uri(sparqlEndpoint));
            ReadWriteSparqlConnector = new ReadWriteSparqlConnector(QueryEndpoint, UpdateEndpoint);
        }
    }
}
