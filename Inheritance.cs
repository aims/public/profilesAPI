﻿using Microsoft.AspNetCore.Mvc;
using Nest;
using System.Collections.Generic;
using System;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using VDS.RDF.Query.Datasets;
using VDS.RDF.Shacl;
using VDS.RDF.Writing;
using AIMS;
using System.Linq;
using Lucene.Net.Search;
using System.Collections;

namespace AIMSapi
{

    public class Inheritance
    {

        IGraph Children;
        IGraph InheritanceG;
        VirtuosoConnector c;
        MimeType MT;
        InMemoryDataset dsap;
        ISparqlQueryProcessor processorap;
        SparqlQueryParser sparqlparser = new SparqlQueryParser();
        public Inheritance(IGraph Inheritance_G, IGraph Children_G, VirtuosoConnector vc)
        {
            InheritanceG = Inheritance_G;
            Children = Children_G;
            c = vc;
            MT = new();
            dsap = new InMemoryDataset(InheritanceG);
            processorap = new LeviathanQueryProcessor(dsap);

        }
        public void Inheritance_count(Uri u)
        {

            IGraph gr = new Graph();

            c.ReadWriteSparqlConnector.LoadGraph(gr, u);

            foreach (var tt in gr.GetTriplesWithPredicate(new Uri("http://www.w3.org/2002/07/owl#imports")))
                InheritanceG.Assert(tt);
            c.ReadWriteSparqlConnector.SaveGraph(InheritanceG);

            IEnumerable<Triple> ienum = gr.GetTriplesWithPredicate(new Uri("http://www.w3.org/2002/07/owl#imports"));
            foreach (var element in ienum)
            {
                IEnumerable<Triple> num = Children.GetTriplesWithSubject(element.Object);
                if (num.Count() == 0)
                {
                    ILiteralNode o = Children.CreateLiteralNode("2");
                    IUriNode p = Children.CreateUriNode(UriFactory.Create("https://www.w3.org/number"));
                    Children.Assert(new Triple(Tools.CopyNode(element.Object, Children), p, o));
                    c.ReadWriteSparqlConnector.SaveGraph(Children);
                }
                else
                    foreach (var l in num)
                    {
                        int inte = int.Parse(l.Object.ToString());
                        Children.Retract(l);
                        c.ReadWriteSparqlConnector.SaveGraph(Children);
                        ILiteralNode o = Children.CreateLiteralNode((inte + 1).ToString());
                        IUriNode p = Children.CreateUriNode(UriFactory.Create("https://www.w3.org/number"));
                        Children.Assert(new Triple(Tools.CopyNode(element.Object, Children), p, o));
                        c.ReadWriteSparqlConnector.SaveGraph(Children);
                    }

            }

        }

        public IEnumerable<Uri> getChildren(String query)
        {
            SparqlQuery squery = sparqlparser.ParseFromString(

            @"SELECT DISTINCT ?s  WHERE {{?s ?p ?o . ?s <http://www.w3.org/2002/07/owl#imports> + <" + query + @"> . }} ");

            SparqlResultSet results = (SparqlResultSet)processorap.ProcessQuery(squery);

            return results.Select(x => new Uri(x.Value("s").ToString()));
        }


    }
}
