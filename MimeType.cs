﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDS.RDF;
using VDS.RDF.JsonLd;
using VDS.RDF.Writing;
using VDS.RDF.Parsing;
using JsonLD.Core;
using JsonLD.Impl;

namespace AIMSapi
{
    public class MimeType
    {
    public String TypeConvertor(String mimetype, IGraph g)

    {
        String data;
        if (mimetype == "text/json-ld") {
                var tripleStore = new TripleStore();
                tripleStore.Add(g);
                var nQuads = VDS.RDF.Writing.StringWriter.Write(tripleStore, new NQuadsWriter());
                var contextMap = JObject.Parse(@"{ '@context' : {}}");
                var parser = new NQuadRDFParser();
                var rdfDataset = parser.Parse(nQuads);
                var jArray = new JsonLdApi().FromRDF(rdfDataset);
                var jObjectCompacted = JsonLD.Core.JsonLdProcessor.Compact(jArray, new Context(contextMap), new JsonLdOptions());
                data = jObjectCompacted.ToString();
            }
        else
        {
                IRdfWriter writer = MimeTypesHelper.GetWriter(mimetype);
                data = VDS.RDF.Writing.StringWriter.Write(g, writer);
        }
        
        return data;
        }

    }
}
