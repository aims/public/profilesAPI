﻿using AIMS;

using OaiPmhNet;
using System;

using System.Linq;

using System.Xml.Linq;
using VDS.RDF;


namespace AIMSapi
{
    public class oai
    {
        //  SparqlQueryParser parser ;
        //  ISparqlQueryProcessor processor;
        //   IGraph idrep;
        IGraph statesGraph;
        VirtuosoConnector vc;

        public oai(VirtuosoConnector c, IGraph states)
        {
            //  parser = sparqlparser;
            //  processor = processorap;
            vc = c;
            //           idrep = idrepo;
            statesGraph = states;
        }

        public String identify()
        {
            // https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2?verb=identify
            XDocument srcTree = new XDocument(new XDeclaration("1.0", "utf-8", "no"),
            // new XComment("This is a comment"),

            new XElement(OaiNamespaces.OaiNamespace + "OAI-PMH",
            //  new XAttribute("xmlns", "http://www.openarchives.org/OAI/2.0/"),

            new XAttribute(XNamespace.Xmlns + "xsi", OaiNamespaces.XsiNamespace),
            new XAttribute(OaiNamespaces.XsiNamespace + "schemaLocation", OaiNamespaces.OaiSchemaLocation),

            new XElement(OaiNamespaces.OaiNamespace + "responseDate", DateTime.UtcNow),
            // change
            new XElement(OaiNamespaces.OaiNamespace + "request", new XAttribute("verb", OaiVerb.Identify), "https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2"),
            new XElement(OaiNamespaces.OaiNamespace + "Identify",
            new XElement(OaiNamespaces.OaiNamespace + "repositoryName", "AIMS"),
            // change
            new XElement(OaiNamespaces.OaiNamespace + "baseURL", "https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2"),
            new XElement(OaiNamespaces.OaiNamespace + "protocolVersion", "2.0"),
            new XElement(OaiNamespaces.OaiNamespace + "adminEmail", "aims@tu-darmstadt.de"),
            // assumption
            new XElement(OaiNamespaces.OaiNamespace + "earliestDatestamp", "2021-01-01T03:49:22.950Z"),
            new XElement(OaiNamespaces.OaiNamespace + "deletedRecord", "transient"),
            new XElement(OaiNamespaces.OaiNamespace + "granularity", "YYYY-MM-DDThh:mm:ssZ")
        )
    )
);
            // return srcTree.ToString();
            return srcTree.ToString();
        }

        /*
        [HttpGet("oai2/getrecord")]

        public profile getrecord(String query)
        {

            IGraph g = new Graph();
            c.ReadWriteSparqlConnector.LoadGraph(g, new Uri(query));
            profile ap = new profile();
            ap.title = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")).First().Object.ToString();
            ap.description = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")).First().Object.ToString();
            ap.creator = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString(); ;
            ap.created = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString(); ;

            return ap;

        }*/


        public String GetRecord(string identifier, string metadataPrefix)
        {
            if (statesGraph.GetTriplesWithSubject(UriFactory.Create(identifier)).First().Object.ToString() != "0")
            {
                var uri = identifier;
                IGraph g = new Graph();
                vc.ReadWriteSparqlConnector.LoadGraph(g, uri);

                //   var iden = string.Join("", new Regex("[0-9]").Matches(identifier));
                //   var n = g.CreateLiteralNode(iden);

                //   g.Clear();

                //var uri = new Uri(idrep.GetTriplesWithObject(n).First().Subject.ToString());
                vc.ReadWriteSparqlConnector.LoadGraph(g, uri);
                APModel ap = new APModel();

                foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))
                {
                    string n = item.Object.ToString();
                    if (n.Contains("@en"))
                    {
                        ap.name = n.Replace("@en", "");
                    }
                    
                }

                foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")))
                {
                    string n = item.Object.ToString();
                    if (n.Contains("@en"))
                    {
                        ap.description = n.Replace("@en", "");
                    }

                }


                ap.creator = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString(); ;
                ap.created = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString(); ;

                XDocument srcTree = new XDocument(new XDeclaration("1.0", "utf-8", "no"),
                // new XComment("This is a comment"),
                new XElement(OaiNamespaces.OaiNamespace + "OAI-PMH",
                new XAttribute(XNamespace.Xmlns + "xsi", OaiNamespaces.XsiNamespace),
                new XAttribute(OaiNamespaces.XsiNamespace + "schemaLocation", OaiNamespaces.OaiSchemaLocation),
                new XElement(OaiNamespaces.OaiNamespace + "responseDate", DateTime.UtcNow),
                new XElement(OaiNamespaces.OaiNamespace + "request", new XAttribute("verb", OaiVerb.GetRecord),
                //uri = identifier.  must be oai_dc
                new XAttribute("identifier", identifier), new XAttribute("metadataPrefix", "oai_dc"),
                //change
                "https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2"),
                new XElement(OaiNamespaces.OaiNamespace + "GetRecord",
                new XElement(OaiNamespaces.OaiNamespace + "record",
                new XElement(OaiNamespaces.OaiNamespace + "header",
                new XElement(OaiNamespaces.OaiNamespace + "identifier", identifier),
                // dateformat maybe isnot right
                new XElement(OaiNamespaces.OaiNamespace + "datestamp", DateTime.UtcNow)),
                new XElement(OaiNamespaces.OaiNamespace + "metadata",
                new XElement(OaiNamespaces.OaiDcNamespace + "dc",
                        new XAttribute(XNamespace.Xmlns + "oai_dc", OaiNamespaces.OaiDcNamespace),
                        new XAttribute(XNamespace.Xmlns + "dc", OaiNamespaces.DcNamespace),
                        new XAttribute(XNamespace.Xmlns + "xsi", OaiNamespaces.XsiNamespace),
                        new XAttribute(OaiNamespaces.XsiNamespace + "schemaLocation", OaiNamespaces.OaiDcSchemaLocation),
                        new XElement(OaiNamespaces.DcNamespace + "title", ap.name),
                        new XElement(OaiNamespaces.DcNamespace + "creator", ap.creator),//.Substring(0, 10)
                        new XElement(OaiNamespaces.DcNamespace + "date", (DateTime.Parse((ap.created).Substring(0, 10))),
                        new XElement(OaiNamespaces.DcNamespace + "description", ap.description),
                        //    new XElement(OaiNamespaces.DcNamespace + "subject", "Fluid Mechanics"),
                        //    new XElement(OaiNamespaces.DcNamespace + "subject", "https://github.com/tibonto/dfgfo/404"),
                        new XElement(OaiNamespaces.DcNamespace + "license", "https://spdx.org/licenses/CC0-1.0.html"),
                        new XElement(OaiNamespaces.DcNamespace + "identifier", uri),
                        new XElement(OaiNamespaces.DcNamespace + "identifier", "https://pg4aims.ulb.tu-darmstadt.de/AIMS/application-profiles/XML/" + System.Net.WebUtility.UrlEncode(uri) + "?includeDefinition=true"),
                        new XElement(OaiNamespaces.DcNamespace + "type", "Application profile"),
                        new XElement(OaiNamespaces.DcNamespace + "format", "https://www.iana.org/assignments/media-types/text/xml")
                )))))));

                // return srcTree.ToString();
                return srcTree.ToString();
            }
            else { return "Private AP!"; }
        }



        public XElement ListId()
        {
            /*
            SparqlQuery squery = parser.ParseFromString(

             @"SELECT DISTINCT ?s  WHERE {?s ?p ?o . ?s a <http://www.w3.org/ns/shacl#NodeShape>.}");
            SparqlResultSet results = (SparqlResultSet)processor.ProcessQuery(squery);

            var rs = results.Select(x => (x.Value(@"s").ToString()));*/


            var xe = new XElement(OaiNamespaces.OaiNamespace + "ListIdentifiers");


            foreach (Triple i in statesGraph.Triples)
            {
                if (i.Object.ToString() != "0")
                {
                    IGraph g = new Graph();

                    try
                    {
                        vc.ReadWriteSparqlConnector.LoadGraph(g, i.Subject.ToString());
                        String dt = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString();
                        //   String id = idrep.GetTriplesWithSubject(new Uri(i)).First().Object.ToString();

                        xe.Add(new XElement(OaiNamespaces.OaiNamespace + "header",
                                new XElement(OaiNamespaces.OaiNamespace + "identifier", i.Subject.ToString()),
                                new XElement(OaiNamespaces.OaiNamespace + "datestamp", DateTime.Parse((dt).Substring(0, 10)))
                                 ));
                    }
                    catch
                    {
                    }
                }
            }
            return xe;
        }


        public String ListIdentifiers(string metadataPrefix)
        {
            //from until must be developed
            // date convertor
            DateTime fromDate = DateTime.MinValue;
            DateTime untilDate = DateTime.MinValue;

            XDocument srcTree = new XDocument(new XDeclaration("1.0", "utf-8", "no"),
            // new XComment("This is a comment"),
            new XElement(OaiNamespaces.OaiNamespace + "OAI-PMH",
            new XAttribute(XNamespace.Xmlns + "xsi", OaiNamespaces.XsiNamespace),
            new XAttribute(OaiNamespaces.XsiNamespace + "schemaLocation", OaiNamespaces.OaiSchemaLocation),
            new XElement(OaiNamespaces.OaiNamespace + "responseDate", DateTime.UtcNow),
            new XElement(OaiNamespaces.OaiNamespace + "request", new XAttribute("verb", OaiVerb.ListIdentifiers),
            //uri = identifier.  must be oai_dc
            new XAttribute("metadataPrefix", "oai_dc"),
            //change
            "https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2"), ListId()));

            //return srcTree.ToString();
            return srcTree.ToString();


        }



        public XElement Listrecord(DateTime? from, DateTime? until)
        {

            /*    SparqlQuery squery = parser.ParseFromString(

                 @"SELECT DISTINCT ?s  WHERE {?s ?p ?o . ?s a <http://www.w3.org/ns/shacl#NodeShape>.}");
                SparqlResultSet results = (SparqlResultSet)processor.ProcessQuery(squery);

                var rs = results.Select(x => (x.Value(@"s").ToString()));

                */
            var xe = new XElement(OaiNamespaces.OaiNamespace + "ListRecords");


            //  foreach (String i in rs)
            foreach (Triple i in statesGraph.Triples)
            {
                if (i.Object.ToString() != "0")
                {


                    IGraph g = new Graph();

                    try
                    {

                        vc.ReadWriteSparqlConnector.LoadGraph(g, new Uri(i.Subject.ToString()));
                        APModel ap = new APModel();

                        foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/title")))
                        {
                            string n = item.Object.ToString();
                            if (n.Contains("@en"))
                            {
                                ap.name = n.Replace("@en", "");
                            }

                        }

                        foreach (var item in g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/description")))
                        {
                            string n = item.Object.ToString();
                            if (n.Contains("@en"))
                            {
                                ap.description = n.Replace("@en", "");
                            }

                        }

                        ap.creator = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/creator")).First().Object.ToString();
                        ap.created = g.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/created")).First().Object.ToString();
                        if (DateTime.Parse((ap.created).Substring(0, 10)) >= from && DateTime.Parse((ap.created).Substring(0, 10)) <= until)
                        //   String id = idrep.GetTriplesWithSubject(new Uri(i)).First().Object.ToString();
                        {
                            xe.Add(new XElement(OaiNamespaces.OaiNamespace + "record",
                                    new XElement(OaiNamespaces.OaiNamespace + "header",
                                    //      new XElement(OaiNamespaces.OaiNamespace + "identifier", "oai:ulb.tu-darmstadt.de:aims/"),
                                    new XElement(OaiNamespaces.OaiNamespace + "identifier", i.Subject.ToString()),
                                    new XElement(OaiNamespaces.OaiNamespace + "datestamp", DateTime.Parse((ap.created).Substring(0, 10)).ToString("yyyy-MM-dd"))
                                     ),
                                    new XElement(OaiNamespaces.OaiNamespace + "metadata",
                    new XElement(OaiNamespaces.OaiDcNamespace + "dc",
                            new XAttribute(XNamespace.Xmlns + "oai_dc", OaiNamespaces.OaiDcNamespace),
                            new XAttribute(XNamespace.Xmlns + "dc", OaiNamespaces.DcNamespace),
                            new XAttribute(XNamespace.Xmlns + "xsi", OaiNamespaces.XsiNamespace),
                            new XAttribute(OaiNamespaces.XsiNamespace + "schemaLocation", OaiNamespaces.OaiDcSchemaLocation),
                            new XElement(OaiNamespaces.DcNamespace + "title", ap.name),
                            new XElement(OaiNamespaces.DcNamespace + "creator", ap.creator),
                            new XElement(OaiNamespaces.DcNamespace + "date", DateTime.Parse((ap.created).Substring(0, 10)).ToString("yyyy-MM-dd")),
                            new XElement(OaiNamespaces.DcNamespace + "description", ap.description),
                            //   new XElement(OaiNamespaces.DcNamespace + "subject", "Fluid Mechanics"),
                            //   new XElement(OaiNamespaces.DcNamespace + "subject", "https://github.com/tibonto/dfgfo/404"),
                            //new XElement(OaiNamespaces.DcNamespace + "license", "https://spdx.org/licenses/CC0-1.0.html"),
                            new XElement(OaiNamespaces.DcNamespace + "identifier", i.Subject.ToString()),
                            new XElement(OaiNamespaces.DcNamespace + "identifier", "https://pg4aims.ulb.tu-darmstadt.de/AIMS/application-profiles/XML/" + System.Net.WebUtility.UrlEncode(i.Subject.ToString()) + "?includeDefinition=true"),
                            new XElement(OaiNamespaces.DcNamespace + "type", "Application profile"),
                            new XElement(OaiNamespaces.DcNamespace + "format", "https://www.iana.org/assignments/media-types/text/xml")
                            ))));
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return xe;
        }

        public String ListRecords(string metadataPrefix, DateTime? from, DateTime? until)
        {
            //from until must be developed
            // date convertor
            //   DateTime fromDate = DateTime.MinValue;
            //   DateTime untilDate = DateTime.MinValue;

            XDocument srcTree = new XDocument(new XDeclaration("1.0", "utf-8", "no"),
            // new XComment("This is a comment"),
            new XElement(OaiNamespaces.OaiNamespace + "OAI-PMH",
            new XAttribute(XNamespace.Xmlns + "xsi", OaiNamespaces.XsiNamespace),
            new XAttribute(OaiNamespaces.XsiNamespace + "schemaLocation", OaiNamespaces.OaiSchemaLocation),
            new XElement(OaiNamespaces.OaiNamespace + "responseDate", DateTime.UtcNow),
            new XElement(OaiNamespaces.OaiNamespace + "request", new XAttribute("verb", OaiVerb.ListRecords),
            //uri = identifier.  must be oai_dc
            new XAttribute("metadataPrefix", "oai_dc"),
            //change
            "https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2"), Listrecord(from, until)));

            //return srcTree.ToString();
            return srcTree.ToString();

        }

        public String ListMetadataFormats()
        {
            XDocument srcTree = new XDocument(new XDeclaration("1.0", "utf-8", "no"),
            // new XComment("This is a comment"),
            new XElement(OaiNamespaces.OaiNamespace + "OAI-PMH",
            new XAttribute(XNamespace.Xmlns + "xsi", OaiNamespaces.XsiNamespace),
            new XAttribute(OaiNamespaces.XsiNamespace + "schemaLocation", OaiNamespaces.OaiSchemaLocation),
            new XElement(OaiNamespaces.OaiNamespace + "responseDate", DateTime.UtcNow),
            new XElement(OaiNamespaces.OaiNamespace + "request", new XAttribute("verb", OaiVerb.ListMetadataFormats),

            "https://pg4aims.ulb.tu-darmstadt.de/AIMS/oai2"),

            new XElement(OaiNamespaces.OaiNamespace + "ListMetadataFormats",
            new XElement(OaiNamespaces.OaiNamespace + "metadataFormat",
            new XElement(OaiNamespaces.OaiNamespace + "metadataPrefix", "oai_dc"),
            new XElement(OaiNamespaces.OaiNamespace + "schema", "http://www.openarchives.org/OAI/2.0/oai_dc.xsd"),
            new XElement(OaiNamespaces.OaiNamespace + "metadataNamespace", "http://www.openarchives.org/OAI/2.0/oai_dc/")

            ))));

            return srcTree.ToString();
        }

        // time intervals
        // exeptions
    }
}
