﻿
using AIMS;
using Elasticsearch.Net;
using Lucene.Net.Search;
using Microsoft.IdentityModel.Tokens;
using Nest;
using System;
using System.Collections.Generic;
using static Lucene.Net.Util.AttributeSource;

namespace AIMSapi
{
    public class ElasticSearch
    {

        ElasticClient client;

        public ElasticSearch(ElasticClient c)
        {
            client = c;
            config();

        }
        public void config()
        {

            var body = @"{
                ""settings"": {
                    ""analysis"": {
                        ""analyzer"": {
                            ""autocomplete"": {
                                ""tokenizer"": ""autocomplete"",
                                ""filter"": [""lowercase""] },
                            ""autocomplete_search"": {""tokenizer"": ""lowercase"" } },
                                ""tokenizer"": {""autocomplete"": {""type"": ""edge_ngram"",
                                                   ""min_gram"": 1,
                                                   ""max_gram"": 20,
                                                   ""token_chars"": [""letter""]
                 }}}},
                ""mappings"": {
                ""properties"": {
                        ""title"": {
                                    ""type"": ""text"",
                                    ""analyzer"": ""autocomplete"",
                                    ""search_analyzer"": ""autocomplete_search""
                                    },
                        ""quote"": {
                                    ""type"": ""text"",
                                    ""analyzer"": ""autocomplete"",
                                    ""search_analyzer"": ""autocomplete_search""
                                    },
                         ""description"": {
                                    ""type"": ""text"",
                                    ""analyzer"": ""autocomplete"",
                                    ""search_analyzer"": ""autocomplete_search""
                                    },
                         ""creator"": {
                                    ""type"": ""keyword""
                             //       ""analyzer"": ""autocomplete"",
                             //       ""search_analyzer"": ""autocomplete_search""
                        },
                        ""state"": {
                                    ""type"": ""integer""
                                   
                        },
                        ""subject"": {
                                    ""type"": ""keyword""
                                   
                        },
                        ""date"": {
                                    ""type"": ""date""
                                   
                        },
                        ""children"": {
                                    ""type"": ""rank_feature""
                                   
                        }
                        ,
                        ""community"": {
                                    ""type"": ""keyword""
                                   
                        }                      
                    }
                }
            }"
        ;

            var response = client.LowLevel.Indices.Create<StringResponse>("aims", body);

            var bodyDE = @"{
                ""settings"": {
                    ""analysis"": {
                        ""filter"": {
                        ""german_stop"": {
                          ""type"":       ""stop"",
                          ""stopwords"":  ""_german_"" 
                        },
        
                        ""german_stemmer"": {
                          ""type"":       ""stemmer"",
                          ""language"":   ""light_german""
                        }
                      },
                        ""analyzer"": {
                            ""autocomplete"": {
                                ""tokenizer"": ""autocomplete"",
                                ""filter"": [""lowercase"",""german_stop"",""german_normalization"",
            ""german_stemmer""
          ] },
                            ""autocomplete_search"": {""tokenizer"": ""lowercase"",
                                                      ""filter"": [
                                    ""lowercase"",
                                    ""german_stop"",
            
                                    ""german_normalization"",
                                    ""german_stemmer""
                                  ]
                                } },
                                ""tokenizer"": {""autocomplete"": {""type"": ""edge_ngram"",
                                                   ""min_gram"": 1,
                                                   ""max_gram"": 20,
                                                   ""token_chars"": [""letter""]
                 }}}},
                ""mappings"": {
                ""properties"": {
                        ""title"": {
                                    ""type"": ""text"",
                                    ""analyzer"": ""autocomplete"",
                                    ""search_analyzer"": ""autocomplete_search""
                                    },
                        ""quote"": {
                                    ""type"": ""text"",
                                    ""analyzer"": ""autocomplete"",
                                    ""search_analyzer"": ""autocomplete_search""
                                    },
                         ""description"": {
                                    ""type"": ""text"",
                                    ""analyzer"": ""autocomplete"",
                                    ""search_analyzer"": ""autocomplete_search""
                                    },
                         ""creator"": {
                                    ""type"": ""keyword""
                          //          ""analyzer"": ""autocomplete"",
                          //          ""search_analyzer"": ""autocomplete_search""
                        },
                        ""state"": {
                                    ""type"": ""integer""
                                   
                        },
                        ""subject"": {
                                    ""type"": ""keyword""
                                   
                        },
                        ""date"": {
                                    ""type"": ""date""
                                   
                        },
                        ""children"": {
                                    ""type"": ""rank_feature""
                                   
                        }

                        ,""community"": {
                                    ""type"": ""keyword""
                                   
                        }     
                    }
                }
            }"
        ;

            var response2 = client.LowLevel.Indices.Create<StringResponse>("aimsde", bodyDE);

        }
        //string subject,DateTime fromDate, DateTime toDate,
        /*   public ISearchResponse<Esearch> autocomplete(String query, string language, int state, bool includeDefinition)
           {

               /*  if (language == "EN")
                 {
                     var searchResponse = client.Search<Esearch>(s => s

                        .From(0)
                        .Size(100)
                        .Index("aims")
                        .Query(q => q
                        .Bool(b => b
                        .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                     //   .Filter(i => i.Term(t => t.Field(f => f.children).Value(c)))
                        .Filter(i => i.DateRange(dr => dr
                                 .Field(f => f.date)
                                 .GreaterThanOrEquals(fromDate)
                                 .LessThanOrEquals(toDate)))
                        .Should(s => s.RankFeature(f => f.Field(t => t.children)))
                        .Must(m => m.Match(t => t.Field(f => f.title)
                        .Query(query))

                        ))));

                     return searchResponse;
                 }
                 */
        /*     if (language == "DE")
             {
                 var searchResponse = client.Search<Esearch>(s => s

                    .From(0)
                    .Size(100)
                    .Index("aimsde")
                    .Query(q => q
                    .Bool(b => b
                    .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                    .Should(s => s.RankFeature(f => f.Field(t => t.children)))
                    .Must(m => m.Match(t => t.Field(f => f.title)
                    .Query(query)
                     )

                    ))));

                 return searchResponse;
             }
             else
             {
                 return client.Search<Esearch>(s => s
                 .From(0)
                    .Size(100)
                    .Index("aims")
                    .Query(q => q
                    .Bool(b => b
                    .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                    .Should(s => s.RankFeature(f => f.Field(t => t.children)))
                    .Must(m => m.Match(t => t.Field(f => f.title)
                    .Query(query)
                     )))));

             }
         }
 */
        public ISearchResponse<Esearch> multifield(String title, String description, String creator, string language, DateTime? from, DateTime? to, string subject, string community, int state, bool includeDefinition)
        {
            /* if (language == "EN")
             {
                 var searchResponse = client.Search<Esearch>(s => s

                    .From(0)
                    .Size(100)
                    .Index("aims")
                    .Query(q => q
                    .Bool(b => b
                    .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                    //   .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                    .Must(
                     bs => bs.Term(p => p.title, title),
                     bs => bs.Term(p => p.description, description),
                     bs => bs.Term(p => p.creator, creator))
               //      bs => bs.Term(p => p.community, "nfdi"))
                    )));

                 return searchResponse;
             }
            */
            if (language == "DE")
            {
                var searchResponse = client.Search<Esearch>(s => s

                   .From(0)
                   .Size(100)
                   .Index("aimsde")
                   .Query(q => q
                   .Bool(b => b
                   .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                   .Filter(i => i.DateRange(dr => dr
                            .Field(f => f.date)
                            .GreaterThanOrEquals(from)
                            .LessThanOrEquals(to)))
                   .Should(s => s.RankFeature(f => f.Field(t => t.children)))
                   .Must(
                    bs => bs.Term(p => p.title, title),
                    bs => bs.Term(p => p.description, description),
                    bs => bs.Term(p => p.creator, creator),
                    bs => bs.Term(p => p.subject, subject),
                    bs => bs.Term(p => p.community, community)

                    ))));

                return searchResponse;
            }
            else
            {
                return client.Search<Esearch>(s => s
                .From(0)
                   .Size(100)
                   .Index("aims")
                   .Query(q => q
                   .Bool(b => b
                   .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                   .Filter(i => i.DateRange(dr => dr
                            .Field(f => f.date)
                            .GreaterThanOrEquals(from)
                            .LessThanOrEquals(to)))
                   .Should(s => s.RankFeature(f => f.Field(t => t.children)))
                   .Must(
                    bs => bs.Term(p => p.title, title),
                    bs => bs.Term(p => p.description, description),
                    bs => bs.Term(p => p.creator, creator),
                    bs => bs.Term(p => p.subject, subject),
                    bs => bs.Term(p => p.community, community)

                    ))));
            }
        }

        public ISearchResponse<Esearch> autocomplete(String title, String description, String creator, string language, DateTime? from, DateTime? to, string subject, string community, int state, bool includeDefinition)
        {

            if (language == "DE")
            {
                var searchResponse = client.Search<Esearch>(s => s

                   .From(0)
                   .Size(100)
                   .Index("aimsde")
                   .Query(q => q
                   .Bool(b => b
                   //      .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                   .Filter(i => i.DateRange(dr => dr
                            .Field(f => f.date)
                            .GreaterThanOrEquals(from)
                            .LessThanOrEquals(to)))
                   .Should(s => s.RankFeature(f => f.Field(t => t.children)))
                   .Must(
                    bs => bs.Term(p => p.title, title),
                    bs => bs.Term(p => p.description, description),
                    bs => bs.Term(p => p.creator, creator),
                    bs => bs.Term(p => p.subject, subject),
                    bs => bs.Term(p => p.community, community)

                    ))));

                return searchResponse;
            }
            else
            {
                return client.Search<Esearch>(s => s
                .From(0)
                   .Size(100)
                   .Index("aims")
                   .Query(q => q
                   .Bool(b => b
                   //       .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                   .Filter(i => i.DateRange(dr => dr
                            .Field(f => f.date)
                            .GreaterThanOrEquals(from)
                            .LessThanOrEquals(to)))
                   .Should(s => s.RankFeature(f => f.Field(t => t.children)))
                   .Must(
                    bs => bs.Term(p => p.title, title),
                    bs => bs.Term(p => p.description, description),
                    bs => bs.Term(p => p.creator, creator),
                    bs => bs.Term(p => p.subject, subject),
                    bs => bs.Term(p => p.community, community)

                    ))));
            }
        }

        public ISearchResponse<Esearch> FuzzyFullText(String query, string language, string state, bool includeDefinition)
        {
            //prefix
            //title boosted
            //rating
            if (language == "EN")
            {
                var searchResponse = client.Search<Esearch>(s => s

                   .From(0)
                   .Size(100)
                   .Index("aims")
                   .Query(q => q
                   .Bool(b => b
                   .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                   .Must(m => m.Match(t => t.Field(f => f.quote)
                   .Query(query)
                   .PrefixLength(3)
                   .Fuzziness(Fuzziness.Auto)
                   .Operator(Operator.And)
                    )))));

                return searchResponse;
            }

            else if (language == "DE")
            {
                var searchResponse = client.Search<Esearch>(s => s

                   .From(0)
                   .Size(100)
                   .Index("aimsde")
                   .Query(q => q
                   .Bool(b => b
                   .Filter(i => i.Term(t => t.Field(f => f.state).Value(state)))
                   .Must(m => m.Match(t => t.Field(f => f.quote)
                   .Query(query)
                   .PrefixLength(3)
                   .Fuzziness(Fuzziness.Auto)
                   .Operator(Operator.And)
                    )))));

                return searchResponse;
            }
            else
            {
                return client.Search<Esearch>(s => s
                     .From(0)
                   .Size(100)
                   .Index("aims")
                   .Query(q => q
                   .Bool(b => b

                   .Must(m => m.Match(t => t.Field(f => f.quote)
                   .Query(query)
                   .PrefixLength(3)
                   .Fuzziness(Fuzziness.Auto)
                   .Operator(Operator.And)
                    )))));

            }
        }



    }
}
